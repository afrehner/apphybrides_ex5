import "./App.css";
import Menu from './Menu';
import Home from './Home.js'
import DetailsPokemon from "./DetailsPokemon";
import { Token } from "./Context.js";
import React, {useState} from "react";
import Login from "./Login.js"
import Favorites from "./Favorites.js"
import {
  BrowserRouter,
  Routes,
  Route
} from "react-router-dom";

function App() {
  const [token, setToken] = useState(null)
  return (
    <div className="container">
     <BrowserRouter>
      <Token.Provider value={{token, setToken}}>

          <Menu/>
          <Routes>
            <Route path="/" element={<Home/>}/>
            <Route path="/details/:id" element={<DetailsPokemon/>}/>
            <Route path="/login" element={<Login/>}/>
            <Route path="/favorites" element={<Favorites/>}/>
          </Routes>

      </Token.Provider>
      </BrowserRouter>
    </div>
  )
}
export default App;
