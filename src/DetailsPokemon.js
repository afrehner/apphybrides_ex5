import React, { useEffect, useState, useContext } from "react";
import { Token } from "./Context.js";
import { useParams } from "react-router-dom";
import Pokemon from "./Pokemon";
import PropTypes from "prop-types";

function DetailsPokemon() {
  const { id } = useParams();
  const tokenObject = useContext(Token);
  const [pokemonDetails, setPokemonDetails] = useState(null);
  const [isFavorite, setIsFavorite] = useState(null);

  useEffect(() => {
    function componentDidMount() {
      fetch(`${process.env.REACT_APP_SERVEUR}/api/pokemon?id=${id}`)
        .then((res) => res.json())
        .then((res) => {
          setPokemonDetails(res);
        })
        .catch((err) => {
          console.log(`fetch details ${err.message}`);
        });
    }
    function getIsFavorite() {
      fetch(`${process.env.REACT_APP_SERVEUR}/api/favorites?PokemonId=${id}`, {
        method: "GET",
        headers: { Authorization: tokenObject.token },
      })
        .then((response) => response.json())
        .then((data) => setIsFavorite(data.IsFavorite))
        .catch((err) => console.log(`getIsFavorite ${err}`));
    }
    componentDidMount();
    if (tokenObject.token !== null) {
      getIsFavorite();
    }
  }, [id, isFavorite, tokenObject]);

  async function changeFavorite() {
    fetch(`${process.env.REACT_APP_SERVEUR}/api/favorites?PokemonId=${id}`, {
      method: isFavorite ? "DELETE" : "POST",
      headers: { Authorization: tokenObject.token },
    }).then(setIsFavorite(!isFavorite));
  }

  return (
    <div>
      {pokemonDetails !== null && (
        <div className="section has-text-centered">
          <ul className="content">
            <li>
              {!isFavorite && tokenObject.token !== null && (
                <button onClick={changeFavorite} className="button is-success">
                  add to favorites
                </button>
              )}
              {isFavorite && tokenObject.token !== null && (
                <button onClick={changeFavorite} className="button is-warning">
                  remove from favorites
                </button>
              )}
            </li>
            <li>
              <img src={pokemonDetails.ImgURL} alt={pokemonDetails.Name} />
            </li>
            <li className="title"> {pokemonDetails.Name}</li>
            <li>HP: {pokemonDetails.HP}</li>
            <li>Attack: {pokemonDetails.Attack}</li>
            <li>Defense: {pokemonDetails.Defense}</li>
            <li>Height: {pokemonDetails.Height}</li>
            <li>SpecialAttack: {pokemonDetails.SpecialAttack}</li>
            <li>SpecialDefense: {pokemonDetails.SpecialDefense}</li>
            <li>Speed: {pokemonDetails.Speed}</li>
            <li>Weight: {pokemonDetails.Weight}</li>
            <li>
              <audio controls>
                <source src={pokemonDetails.CryURL} type="audio/ogg" />
              </audio>
            </li>
            <li>Habitat: {pokemonDetails.Habitat.Name}</li>
            <li>Species: {pokemonDetails.Species.Name}</li>
          </ul>
          {
            pokemonDetails.Evolution !== null &&
            <p className="title">Evolutions:</p>
          }
          <div className="columns is-centered margin">
            <div></div>
            {HasEvolution(pokemonDetails, [])}
          </div>
        </div>
      )}
    </div>
  );
}

function HasEvolution({ Evolution }, arr) {
  if (Evolution) {
    arr.push(<Pokemon key={Evolution.PokemonId} pokemon={Evolution} />);
    HasEvolution(Evolution, arr);
  }
  return arr;
}

HasEvolution.propTypes = {
  Evolution: PropTypes.object.isRequired,
};
export default DetailsPokemon;
