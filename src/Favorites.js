import React, {useEffect,useContext, useState} from "react";
import PokemonList from "./PokemonList.js"
import {Token} from "./Context.js"
import {useNavigate} from "react-router-dom";

function GetFavorites() {
  const navigate = useNavigate();
  const [favorites, setFavorites] = useState([])
  const tokenObject = useContext(Token)
  useEffect(() => {
    async function fetchData() {
      fetch(`${process.env.REACT_APP_SERVEUR}/api/favorites`, {
        method: 'GET',
        headers: { Authorization: tokenObject.token }
      })
      .then((res) => res.json()).then(data=>setFavorites(data))
      .catch(err => {
        console.log(err.message)
        navigate('/')
      })
    }
    fetchData()
  }, [tokenObject])

  return (
    <div>
    <p className="has-text-centered title">Favorites </p>
    <PokemonList list={favorites}/>
    </div>
  )
}
export default GetFavorites;
