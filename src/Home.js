import SelectForm from './SelectForm.js';
import React, {useEffect, useState} from "react";
import PokemonList from "./PokemonList.js"

function Home() {
    const [allPokemon, setAllPokemon] = useState([]);
    const [selectedPokeType, setSelectedPokeType] = useState("");
    const [selectedHabitat, setSelectedHabitat]=useState("");
    const [selectedSpecies, setSelectedSpecies]=useState("");

    function handleChangeHabitat(event) {
      setSelectedHabitat(event)
      handleChangePokemon()
    }
    function handleChangePokeType(event) {
      setSelectedPokeType(event)
      handleChangePokemon()
    }
    function handleChangeSpecies(event) {
      setSelectedSpecies(event)
      handleChangePokemon()
    }
    function handleChangePokemon() {
      let pokemonList = allPokemon;
      if (selectedPokeType > 0) {
        pokemonList = pokemonList.filter((p) =>
          p.PokeTypes.find(
            (type) => type.PokeTypeId === Number(selectedPokeType)
          )
        );
      }
      if (selectedHabitat > 0) {
        pokemonList = pokemonList.filter(
          (p) => p.Habitat.HabitatId === Number(selectedHabitat)
        );
      }
      if (selectedSpecies > 0) {
        pokemonList = pokemonList.filter(
          (p) => p.Species.SpeciesId === Number(selectedSpecies)
        );
      }
      return pokemonList;
    }

    useEffect(() => {
      function fetchData() {
        fetch(`${process.env.REACT_APP_SERVEUR}/api/pokemons`)
        .then((res) => res.json()).then(data=>setAllPokemon(data))
      }
      fetchData()
    }, []);
      return (
        <div>
          <div className="section">
            <div className="columns is-centered">
              <SelectForm
                idName="HabitatId"
                filterName="habitats"
                onChange={handleChangeHabitat}
              />
              <SelectForm
                idName="SpeciesId"
                filterName="species"
                onChange={handleChangeSpecies}
              />
              <SelectForm
                idName="PokeTypeId"
                filterName="poketypes"
                onChange={handleChangePokeType}
              />
            </div>
          </div>
          <PokemonList list={handleChangePokemon()}/>
        </div>
      )
  }

  export default Home;
