import { Token } from "./Context.js";
import React, { useState, useContext, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import PropTypes from "prop-types";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEnvelope, faLock } from '@fortawesome/free-solid-svg-icons'


function Login() {
  const navigate = useNavigate();
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [isError, setIsError] = useState(false);
  const tokenObject = useContext(Token);

  useEffect(() => {}, [tokenObject]);

  function loginSuccess() {
    setIsError(false);
    navigate("/favorites");
  }
  function loginFailure(err) {
    tokenObject.setToken(null);
    setUsername("");
    setPassword("");
    setIsError(true);
    console.log(err);
  }
  function goHome() {
    navigate("/");
  }
  function getToken() {
    const encodedUsername = encodeURIComponent(username);
    const encodedPassword = encodeURIComponent(password);
    const bodyContent = `grant_type=password&username=${encodedUsername}&password=${encodedPassword}`;

    fetch(`${process.env.REACT_APP_SERVEUR}/token`, {
      method: "POST",
      body: bodyContent,
      headers: { "Content-Type": "application/x-www-form-urlencoded" },
    })
      .then(
        (response) => {
          if (response.ok) {
            return response.json();
          }
          return Promise.reject("login error");
        },
        (err) => console.error(err)
      )
      .then(
        (data) => {
          tokenObject.setToken(`${data.token_type} ${data.access_token}`);
          loginSuccess();
        },
        (err) => {
          console.error(err);
          loginFailure(err);
        }
      );
  }

  return (
    <section className="section">
      <div className="container">
        <div className="title left-margin">Sign in</div>
        <InputField
          label="username"
          type="text"
          isError={isError}
          onChange={setUsername}
          value={username}
          icon={faEnvelope}
        />
        <InputField
          label="password"
          type="password"
          isError={isError}
          onChange={setPassword}
          value={password}
          icon={faLock}
        />
        {isError && (
          <div className="has-text-danger">Email ou mot de passe incorrect </div>
        )}
        <div className="field is-horizontal" style={{marginBottom: 5}}>
          <div className="field-body">
            <div className="field is-grouped">
              <div className="control">
                <button onClick={getToken} className="button is-link">
                  Connexion
                </button>
              </div>
              <div className="control">
                <button onClick={goHome} className="button is-danger">
                  Annuler
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}

function InputField({ onChange, type, value, label, icon }) {
  function handleChange(event) {
    onChange(event.target.value);
  }
  return (
    <div className="field">
      <div className="label is-normal">
        <label htmlFor="username" className="label">
          {label}
        </label>
      </div>
      <div className="field-body">
        <div className="field">
          <p className="control is-expanded has-icons-left">
            <input
              type={type}
              onChange={handleChange}
              className="input"
              placeholder={label}
              value={value}
            />
            <span className="icon is-small is-left">
              <FontAwesomeIcon icon={icon}></FontAwesomeIcon>
            </span>
          </p>
        </div>
      </div>
    </div>
  );
}
InputField.propTypes = {
  onChange: PropTypes.func.isRequired,
  label: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  icon: PropTypes.object.isRequired
};
export default Login;
