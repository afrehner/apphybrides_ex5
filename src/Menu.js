import { Link } from "react-router-dom";
import {useNavigate} from "react-router-dom";
import {Token } from './Context.js'
import React, {useContext, useEffect} from "react"

function Menu() {
    const tokenObject= useContext(Token)
    const navigate = useNavigate();

    function goLogin() {
        navigate('/login')
    }
    function logout(){
        tokenObject.setToken(null)
    }

    useEffect(() =>{}, [tokenObject])
    return (
        <nav className="navbar" role="navigation" aria-label="main navigation">
            <div className="navbar-menu">
                <div className="navbar-start">
                    <Link to="/" className="navbar-item">Home</Link>
                    {
                        tokenObject.token !== null &&
                        <Link to="/favorites" className="navbar-item">Favorites</Link>
                    }
                </div>
                <div className="navbar-end">
                    <div className="navbar-item">

                        <div className="buttons"> {
                            tokenObject.token === null &&
                            <button className="button is-primary" onClick={goLogin}> Login</button>
                        } {
                            tokenObject.token !== null &&
                            <button className="button is-danger" onClick={logout}> Logout</button>
                        }
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    )
}


export default Menu;
