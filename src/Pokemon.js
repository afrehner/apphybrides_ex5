import React from "react";
import { Link } from "react-router-dom";
import PropTypes from 'prop-types';
import {PokemonObject} from './PokemonObjects.ts'


function Pokemon ({ pokemon }){
  return (
    <div className="column is-2">
    <Link to={`/details/${pokemon.PokemonId}`}>
        <div className="card">
          <div style={{backgroundColor: pokemon.Color}}>
            <div className="card-image">
              <figure className="image is-4by3">
                <img
                  src={pokemon.ImgURL}
                  alt={pokemon.Name}
                  />
              </figure>
            </div>
            <div className="card-content">
              <p className="title is-4">{ pokemon.Name}</p>
              <p className="subtitle is-6"> Species: { pokemon.Species.Name }</p>
              <p className="subtitle is-6"> Habitat: { pokemon.Habitat.Name }</p>
              <p className="subtitle is-6"> PokeType: {pokemon.PokeTypes.map((type) =>type.Name).join(', ')}</p>
            </div>
          </div>
        </div>
    </Link>
  </div>)
  }
Pokemon.propTypes = {
  pokemon: PropTypes.objectOf(PokemonObject).isRequired
}
  export default Pokemon
