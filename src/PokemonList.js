import Pokemon from './Pokemon.js'
import React from 'react';
import PropTypes from 'prop-types';

function PokemonList({list}) {
  return (
    <div>
      <div className="row columns is-multiline">
        {list.map((p) => <Pokemon key={p.PokemonId} pokemon={p}/>)}
      </div>
    </div>
  );
}
PokemonList.propTypes = {
  list: PropTypes.array.isRequired
}

export default PokemonList;
