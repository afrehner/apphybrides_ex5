
export interface PokemonObject {
    PokemonId: number;
    Name:      string;
    Color:     String;
    ImgURL:    string;
    CryURL:    string;
    ThumbURL:  string;
    Habitat:   Habitat;
    Species:   Species;
    PokeTypes: PokeType[];
}

export interface Habitat {
    HabitatId: number;
    Name:      HabitatName;
}

export enum HabitatName {
    Cave = "Cave",
    Forest = "Forest",
    Grassland = "Grassland",
    Mountain = "Mountain",
    RoughTerrain = "Rough terrain",
    Sea = "Sea",
    Urban = "Urban",
    WaterSEdge = "Water's edge",
}

export interface PokeType {
    PokeTypeId: number;
    Name:       string;
}

export interface Species {
    SpeciesId: number;
    Name:      string;
}


