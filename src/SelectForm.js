import React from "react";
import Select from "react-select";
import PropTypes from "prop-types";

function SelectForm({ filterName, idName, onChange }) {
  let listItems = [{ value: 0, label: "Select" }];
  let selected = 0;
  async function fetchData() {
    try {
      let val = "";
      const url = `${process.env.REACT_APP_SERVEUR}/api/${filterName}`;
      await fetch(url)
        .then((res) => res.json())
        .then((data) =>
          data.forEach((item) => {
            val = { value: item[idName], label: item.Name };
            listItems.push(val);
          })
        )
        .catch((err) => console.log(err));
    } catch (err) {
      console.log(`error ${err.message}`);
    }
  }
  fetchData();

  function handleChange(value) {
    selected = value.value;
    onChange(selected);
  }
  return (
    <div className="field is-horizontal" style={{ paddingLeft: "20px" }}>
      <div className="field-label is-normal">
        <label className="label">{filterName}</label>
      </div>
      <div className="field-body">
        <div className="field">
          <div className="control" style={{ minWidth: "200px" }}>
            <Select onChange={handleChange} options={listItems} />
          </div>
        </div>
      </div>
    </div>
  );
}

SelectForm.propTypes = {
  filterName: PropTypes.string.isRequired,
  idName: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
};
export default SelectForm;
